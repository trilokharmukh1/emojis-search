import React from 'react';
import './../style/searchEmoji.css';
import DisplayEmojis from './displayEmojis';

class SeachEmoji extends React.Component {
    state = {
        value:""
    };

    getEmojiName = (event) =>{
        this.setState({value: event.target.value});
    }
        
    render() {
        console.log(this.state);    
        return (
            <div>
                <input className="search" type="text" placeholder="Search emojis" onChange={this.getEmojiName} />
                <DisplayEmojis emojisList={this.state} />
            </div>



        );
    }
}

export default SeachEmoji;