import './App.css';
import SearchEmoji from './components/searchEmoji';
function App() {
  return (
    <div className="App">
      <SearchEmoji/>
    </div>
  );
}

export default App;
