import React from 'react';
import './../style/displayEmojis.css';
import emojisData from './../emojiList.json';

class DisplayEmojis extends React.Component {

    state = {
        copyEmoji: ""
    };
    render() {
        let emojis = "";
        if (this.props.emojisList.value.trim() !== "") {
            emojis = emojisData.filter((emoji) => {
                let arrayOfEmoji = emoji.title.toLowerCase().split(" ");
                return arrayOfEmoji.includes(this.props.emojisList.value.toLowerCase().trim())

            })

            if (emojis.length === 0) {
                emojis = emojisData.filter((emoji) => {
                    return emoji.keywords.includes(this.props.emojisList.value.toLowerCase().trim())
                })
                console.log("inside");
            }
        }
        return (
            <div className="wrapper">
                <div className="emoji-container">

                    {
                        
                        emojis.length > 0
                            ? emojis.map((emoji) => {
                                return <button className="emoji-area" onClick={() => { navigator.clipboard.writeText(emoji.symbol); this.setState({ copyEmoji: "copied" }) }}><div>copied</div>{emoji.symbol}</button>
                            })

                            : this.props.emojisList.value.trim() === ""
                                ? <div className="message">Type a word like happy or smile or heart and we’ll show you matching emojis</div>
                                : <div className="message">😦 <br /> Hmm, no matches for that.</div>
                    }

                </div>
            </div>
        );
    }
}

export default DisplayEmojis;